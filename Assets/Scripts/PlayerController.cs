﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    [Header("Defines")]
    [Space(10)]
    public Rigidbody Player;
    public Transform PlayerPos;
    public CursorLockMode CurserMode;
    public Transform PlayerSoul;
    public SphereCollider LegsCollider;
    public Transform MainCam;
    public CinemachineBrain SkyCam;
    public CameraController CamController;
    public GameObject Head;
    [Header("Scripts")]
    [Space(10)]
    public GameController gameController;
    public BuildingPlayerAbility Builder;
    public PlayerUISystem UIsystem;
    public PlayerInput MainInput;

    [Header("Speeds")]
    [Space(10)]
    public float WallJumpForce;
    public float RunningSpeed;
    public float StrafingSpeed;
    public float JumpForce;
    public float maxSpeed = 200f;
    public float sensitivity = 1;
    public float BaseSens;
    public float ScopedSens;
    public float Speed;
    public float ScrollSens;

    [Header("Stats/Debugging")]
    [Space(10)]
    public bool AvoidJitterMapMenu;
    public Vector3 camvelocity;
    public float smoothTime;
    public bool AlreadyLeftWallJumped;
    public bool AlreadyRightWallJumped;
    public float WallJumpLength;
    public Vector3 CameraRotationEuler;
    public Vector3 MainCameraXClampable;
    public float CameraRotationY;
    public int currentWeapon;
    public Transform[] weapons;
    public float Health;
    public float MaxHealth;
    public float Energy;
    public float MaxEnergy;
    public bool Invincable;
    public float Invincabilitytime;
    public int Cash;
    public int MaxCash;
    // Start is called before the first frame update
    void Start()
    {
        UIsystem = gameObject.GetComponent<PlayerUISystem>();
        MaxHealth = Health;
        if (Player == null)
        {
            Player = GetComponent<Rigidbody>();
            Debug.LogError("NO RIGIDBODY CONNECTED TO PLAYER");
        }
        if(PlayerPos == null)
        {
            PlayerPos = GetComponent<Transform>();
            Debug.LogError("TRANSFORM NOT FOUND ON PLAYER");
        }
        BaseSens = sensitivity;
    }
    // Update is called once per frame
    public void LoadSceneFromButton(string scenetobeloaded)
    {
        SceneManager.LoadScene(scenetobeloaded);
    }
    void FixedUpdate()
    {
        MainInput.FunctionKey2 = Input.GetKeyDown(KeyCode.F1);
        MainInput.FunctionKey1 = Input.GetKeyDown(KeyCode.F2);
        CamController.CamMain.transform.position = Vector3.SmoothDamp(CamController.CamMain.transform.position, Head.transform.position, ref camvelocity, smoothTime);
        CamController.CamMain.transform.rotation = Head.transform.rotation;
        //CamController.CamMain.transform.rotation = MainCam.rotation;
        if (MainInput.Leave)
        { 
            
        }
        if(Cash > MaxCash)
        { Cash = MaxCash; }
        if (Health < 0)
        { Health = 0; }
        if (Health > MaxHealth)
        { Health = MaxHealth; }
        MainInput.OpenMapMenu = Input.GetKeyDown(KeyCode.Q);

        CamController.Cam001.m_Lens.FieldOfView = Mathf.Clamp(CamController.Cam001.m_Lens.FieldOfView += MainInput.Scrollwheel * ScrollSens, 1f, 160f);
        MainInput.Scrollwheel = -Input.GetAxis("Mouse ScrollWheel");
        if (MainInput.OpenMapMenu && !MainInput.MapMenuIsOpen && AvoidJitterMapMenu == false)
        {
            AvoidJitterMapMenu = true;
            MainInput.ResetInput(PlayerInput.Resettype.Full);
        }
        if(Input.GetKeyUp(KeyCode.Q))
        { AvoidJitterMapMenu = false;  }
        else if (MainInput.OpenMapMenu && MainInput.MapMenuIsOpen && AvoidJitterMapMenu == false)
        {
            AvoidJitterMapMenu = true;
            Cursor.lockState = CursorLockMode.Locked;
            MainInput.MapMenuIsOpen = false;
            MainInput.CheckForInput = true;
        }
        UIsystem.UpdateUI();
        if (MainInput.CheckForInput && !gameController.Won)
        { MainInput.GetKeys(); }
        if(gameController.Won)
        { MainInput.ResetInput(PlayerInput.Resettype.No_Camera); }
        if (MainInput.MapMenuIsOpen)
        { Builder.PlaceBuildings(); }
        Move();
        Look();
        CheckForWalljump();
        CameraRotationY = PlayerSoul.rotation.x;
        Speed = Player.velocity.magnitude;
        if (Player.velocity.magnitude > maxSpeed)
        {
            Player.velocity = Player.velocity.normalized * maxSpeed;
        }
        if (MainInput.Jump && MainInput.CanJump)
        {
            MainInput.CanJump = false;
            Player.AddForce(0, JumpForce, 0);
        }
    }
    void CheckForWalljump()
    {
        Debug.DrawRay(MainCam.transform.position, MainCam.transform.right * WallJumpLength, new Color(0, 0, 255), 0.5f);
        Debug.DrawRay(MainCam.transform.position, -MainCam.transform.right * WallJumpLength, new Color(0, 0, 255), 0.5f);
        RaycastHit LeftWallJumpPossible;
        RaycastHit RightWallJumpPossible;
        if (Physics.Raycast(MainCam.transform.position, MainCam.transform.right, out RightWallJumpPossible, WallJumpLength))
        {
            if (!AlreadyRightWallJumped)
            {
                if (MainInput.Jump && RightWallJumpPossible.collider.gameObject.CompareTag("Jumpable Wall"))
                {
                    AlreadyRightWallJumped = true;
                    AlreadyLeftWallJumped = false;
                    Player.AddForce(-MainCam.right * JumpForce * WallJumpForce);
                    Player.AddForce(PlayerPos.up * JumpForce * WallJumpForce);
                }
            }
        }
        if (Physics.Raycast(MainCam.transform.position, -MainCam.transform.right, out LeftWallJumpPossible, WallJumpLength))
        {
            if (!AlreadyLeftWallJumped)
            {
                if (MainInput.Jump && LeftWallJumpPossible.collider.gameObject.CompareTag("Jumpable Wall"))
                {
                    AlreadyLeftWallJumped = true;
                    AlreadyRightWallJumped = false;
                    Player.AddForce(-MainCam.right * JumpForce * WallJumpForce);
                    Player.AddForce(PlayerPos.up * JumpForce * WallJumpForce);
                }
            }
        }
    }

    public void SwitchWeapons(int num)
    {
        currentWeapon = num;
        if(num > weapons.Length)
        { return; }
        if (num < 0)
        { return; }
        for (int i = 0; i < weapons.Length; i++)
        {
            if (i == num)
                weapons[i].gameObject.SetActive(true);
            else
                weapons[i].gameObject.SetActive(false);
        }
    }
    void Look()
    {
        Player.transform.Rotate(0, MainInput.MouseHor * sensitivity, 0);
        CameraRotationEuler = new Vector3(MainInput.MouseVer * -sensitivity, 0, 0);
        Head.transform.Rotate(CameraRotationEuler);
    }
    void Move()
    {
        Player.AddForce(PlayerPos.forward * MainInput.VerticalAxis * RunningSpeed);
        Player.AddForce(PlayerPos.right * MainInput.HorizontalAxis * StrafingSpeed);
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.transform.rotation.x != 0 || collision.gameObject.transform.rotation.y != 0)
        {
            Player.velocity *= 1.2f;
            AttemptJump(collision);
            return;
        }
        else
        {
            AttemptJump(collision);
            return;
        }

    }
    public IEnumerator TakeDamage()
    {
        Invincable = true;
        yield return new WaitForSecondsRealtime(Invincabilitytime);
        Invincable = false;
    }
    void AttemptJump(Collision collision)
    {
        if (collision.collider.CompareTag("Ground"))
        {
            AlreadyRightWallJumped = false;
            AlreadyLeftWallJumped = false;
            MainInput.CanJump = true;
            return;
        }
        MainInput.CanJump = false;
    }
}
