﻿using UnityEngine;

public class Powerups : MonoBehaviour
{
    public enum PowerupType { HealthBox, AmmoBox, SpeedUp, Frenzy };
    public enum PowerupSize { Small, Medium, Large, FULL_RESTORE, CUSTOM };
    [Header("Defines")]
    [Space(10)]
    public GameObject Player;
    public GameObject PowerupImage;
    public GameObject PowerupText;
    [Header("Stats")]
    [Space(10)]
    public PowerupType TypeOfPowerup;
    public PowerupSize SizeOfPowerup;
    public string Description;
    [Header("Only used if PowerupSize is set to CUSTOM")]
    public float SpeedValue;
    public float maxSpeedValue;
    public float HealthValue;
    public float maxHealthValue;
    public float AmmoCount;
    public float ClipCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (TypeOfPowerup == PowerupType.AmmoBox)
            { AmmoUP(collision); }
            if (TypeOfPowerup == PowerupType.HealthBox)
            { HealthUp(collision); }
            if (TypeOfPowerup == PowerupType.SpeedUp)
            { SpeedUp(collision); }
            if (TypeOfPowerup == PowerupType.Frenzy)
            {
                //Not implemented yet
                return;
            }
        }

    }
    void SpeedUp(Collider player)
    {
        if (SizeOfPowerup == PowerupSize.Small)
        {
            player.gameObject.GetComponent<Rigidbody>().velocity *= 1.2f;
            player.gameObject.GetComponent<PlayerController>().maxSpeed += 0.5f;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.Medium)
        {
            player.gameObject.GetComponent<Rigidbody>().velocity *= 1.6f;
            player.gameObject.GetComponent<PlayerController>().maxSpeed += 1;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.Large)
        {
            player.gameObject.GetComponent<Rigidbody>().velocity *= 2.3f;
            player.gameObject.GetComponent<PlayerController>().maxSpeed += 2;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.FULL_RESTORE)
        {
            player.gameObject.GetComponent<Rigidbody>().velocity *= 300f;
            Destroy(this.gameObject);
        }
        if(SizeOfPowerup == PowerupSize.CUSTOM)
        {
            player.gameObject.GetComponent<Rigidbody>().velocity *= SpeedValue;
            player.gameObject.GetComponent<PlayerController>().maxSpeed += maxSpeedValue;
            Destroy(this.gameObject);
        }
    }
    void HealthUp(Collider player)
    {
        if (SizeOfPowerup == PowerupSize.Small)
        {
            player.gameObject.GetComponent<PlayerController>().Health += 10;
            player.gameObject.GetComponent<PlayerController>().MaxHealth += 5;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.Medium)
        {
            player.gameObject.GetComponent<PlayerController>().Health += 30;
            player.gameObject.GetComponent<PlayerController>().MaxHealth += 10;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.Large)
        {
            player.gameObject.GetComponent<PlayerController>().Health += 50;
            player.gameObject.GetComponent<PlayerController>().MaxHealth += 20;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.FULL_RESTORE)
        {
            player.gameObject.GetComponent<PlayerController>().Health = player.gameObject.GetComponent<PlayerController>().MaxHealth;
            Destroy(this.gameObject);
        }
        if (SizeOfPowerup == PowerupSize.CUSTOM)
        {
            player.gameObject.GetComponent<PlayerController>().Health += HealthValue;
            player.gameObject.GetComponent<PlayerController>().MaxHealth += maxHealthValue;
            Destroy(this.gameObject);
        }
    }
    void AmmoUP(Collider player)
    {
        if (SizeOfPowerup == PowerupSize.Small)
        { player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Clips += 10; Destroy(this.gameObject); }
        if (SizeOfPowerup == PowerupSize.Medium)
        { player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Clips += 30; Destroy(this.gameObject); }
        if (SizeOfPowerup == PowerupSize.Large)
        { player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Clips += 60; Destroy(this.gameObject); }
        if (SizeOfPowerup == PowerupSize.FULL_RESTORE)
        { 
            player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Clips = 
            player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.MaxClips;
            player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Ammo = 
            player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Ammo;
            Destroy(this.gameObject); 
        }
        if (SizeOfPowerup == PowerupSize.CUSTOM)
        {
            player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Clips += Mathf.RoundToInt(ClipCount);
            player.GetComponent<PlayerController>().weapons[player.GetComponent<PlayerController>().currentWeapon].gameObject.GetComponent<Gun>().GunData.Ammo += Mathf.RoundToInt(AmmoCount);
            Destroy(this.gameObject);
        }
    }
}
