﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    public PlayerController PlayerInput;
    public CinemachineVirtualCamera CamMain;
    public CinemachineVirtualCamera Cam001;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(PlayerInput.MainInput.MapMenuIsOpen)
        { Cam001.transform.Translate(Input.GetAxis("Horizontal") * PlayerInput.ScrollSens, Input.GetAxis("Vertical") * PlayerInput.ScrollSens, 0); }
        
        if (PlayerInput.MainInput.MapMenuIsOpen)
        {
            //Cam001.m_Lens.FieldOfView -= PlayerInput.Scrollwheel;
            //PlayerInput.Scrollwheel = Input.GetAxis("Mouse ScrollWheel");
                CamMain.Priority = 1;
                Cam001.Priority = 2;
        }
        else
        {
                CamMain.Priority = 2;
                Cam001.Priority = 1;
        }
    }
}
