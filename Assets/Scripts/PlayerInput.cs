﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PlayerInput : MonoBehaviour
{
    public enum ControlMethod { ArrowKeys, WASD, KeypadArrows }
    [Header("Defines")]
    [Space(10)]
    public PlayerController PlayerC;
    [Header("Input")]
    [Space(10)]
    public ControlMethod MovementControl;
    public bool CheckForInput;
    public float HorizontalAxis;
    public float VerticalAxis;
    public bool Jump;
    public bool Shoot_FullAuto;
    public bool Shoot_SemiAuto;
    public bool Aim;
    public bool CanJump;
    public float MouseHor;
    public float MouseVer;
    public bool crouch;
    public float Scrollwheel;
    public bool NumberKey1;
    public bool NumberKey2;
    public bool NumberKey0;
    public bool NumberKey3;
    public bool NumberKey4;
    public bool NumberKey5;
    public bool Reload;
    public bool OpenMapMenu;
    public bool MapMenuIsOpen;
    public bool FunctionKey1;
    public bool FunctionKey2;
    public bool Leave;
    public void GetKeys()
    {
        if (NumberKey1)
        { PlayerC.SwitchWeapons(1); }
        if (NumberKey2)
        { PlayerC.SwitchWeapons(2); }
        if (NumberKey3)
        { PlayerC.SwitchWeapons(3); }
        if (NumberKey4)
        { PlayerC.SwitchWeapons(0); }
        Cursor.lockState = PlayerC.CurserMode;
        if (MovementControl == ControlMethod.WASD)
        {
            HorizontalAxis = Input.GetAxis("Horizontal");
            VerticalAxis = Input.GetAxis("Vertical");
        }
        Jump = Input.GetButton("Jump");
        Shoot_FullAuto = Input.GetButton("Fire1");
        Shoot_SemiAuto = Input.GetButtonDown("Fire1");
        Aim = Input.GetButton("Fire2");
        MouseHor = Input.GetAxis("Mouse X");
        MouseVer = Input.GetAxis("Mouse Y");
        crouch = Input.GetKey(KeyCode.LeftShift);
        Leave = Input.GetButton("Cancel");
        NumberKey1 = Input.GetKeyDown(KeyCode.Alpha1);
        NumberKey2 = Input.GetKeyDown(KeyCode.Alpha2);
        NumberKey0 = Input.GetKeyDown(KeyCode.Alpha0);
        NumberKey3 = Input.GetKeyDown(KeyCode.Alpha3);
        NumberKey4 = Input.GetKeyDown(KeyCode.Alpha4);
        NumberKey5 = Input.GetKeyDown(KeyCode.Alpha5);
        Reload = Input.GetKey(KeyCode.R);
    }
    public void UseFunctionKeys()
    {
        if (FunctionKey2)
        {
            Debug.Log("Captured Screen to " + System.DateTime.Now.ToString());
            ScreenCapture.CaptureScreenshot(System.DateTime.Now.ToString());
        }
    }

    public enum Resettype { Full, Just_Input, No_Camera}
    public void ResetInput(Resettype reset)
    {
        if (reset == Resettype.Full)
        {
            HorizontalAxis = 0;
            VerticalAxis = 0;
            Jump = false;
            Shoot_FullAuto = false;
            Shoot_SemiAuto = false;
            Aim = false;
            CanJump = false;
            MouseHor = 0;
            MouseVer = 0;
            crouch = false;
            NumberKey1 = false;
            NumberKey2 = false;
            NumberKey0 = false;
            FunctionKey2 = false;
            FunctionKey1 = false;
            Reload = false;
            Cursor.lockState = CursorLockMode.None;

            MapMenuIsOpen = true;
            CheckForInput = false;
        }
        if(reset == Resettype.Just_Input)
        {
            HorizontalAxis = 0;
            VerticalAxis = 0;
            Jump = false;
            Shoot_FullAuto = false;
            Shoot_SemiAuto = false;
            Aim = false;
            CanJump = false;
            MouseHor = 0;
            MouseVer = 0;
            crouch = false;
            NumberKey1 = false;
            NumberKey2 = false;
            NumberKey0 = false;
            FunctionKey2 = false;
            FunctionKey1 = false;
            Reload = false;        }
        if(reset == Resettype.No_Camera)
        {
            HorizontalAxis = 0;
            VerticalAxis = 0;
            Jump = false;
            Shoot_FullAuto = false;
            Shoot_SemiAuto = false;
            Aim = false;
            CanJump = false;
            MouseHor = 0;
            MouseVer = 0;
            crouch = false;
            NumberKey1 = false;
            NumberKey2 = false;
            NumberKey0 = false;
            FunctionKey2 = false;
            FunctionKey1 = false;
            Reload = false;
            Cursor.lockState = CursorLockMode.None;
            CheckForInput = false;
        }
    }
}