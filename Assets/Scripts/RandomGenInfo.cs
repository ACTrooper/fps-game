﻿using UnityEngine;
using System.Collections;

public class RandomGenInfo : ScriptableObject
{
    public enum visualmode { Height, Heat, Moisture, Biome}
    public visualmode VisualizationMode;
    public int NumberTilesY;
    public int NumberTilesX;
}
