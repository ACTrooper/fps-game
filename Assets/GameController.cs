using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public bool Won;
    public Sprite[] ObjectiveArray;
    public GameObject player;
    public GameObject EnemySpawners;
    public int SpawnersRemaining;
    public GameObject[] Spawners;
    public float MissionTime;
    public float Timer;
    public PlayerUISystem playerUISystem;
    public enum GameType {  Ambush, Attack_Defense, Time}
    public GameType Game_Mode;
    public int i;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Main Camera");
        if(Game_Mode == GameType.Ambush)
        {
            playerUISystem.GameObjectiveTypeImage.sprite = ObjectiveArray[0];
        }
        if(Game_Mode == GameType.Time)
        {
            playerUISystem.GameObjectiveTypeImage.sprite = ObjectiveArray[1];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Game_Mode == GameType.Time)
        {
            if (Timer > 0)
            {
                Timer -= Time.deltaTime;
            }
            else
            {
                Won = true;
            }
        }
        if (Game_Mode == GameType.Ambush)
        {
            if (SpawnersRemaining > 0)
            {
                SpawnersRemaining = EnemySpawners.GetComponent<EnemySpawner>().SpawningLocations.Count;
            }
            else
            {
                playerUISystem.GameObjectivesRemaining.text = "No Enemy Spawners Remaining!";
                Won = true;
            }
        }
    }
}