﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
[CreateAssetMenu(fileName = "Gun", menuName = "Custom Objects/Gun", order = 1)]
public class GunScriptableObject : ScriptableObject
{
    public enum ProjectileType { SelfProppeledBullet, RayCast, Conecast }
    public enum GunType { FullAuto, SemiAuto}
    public enum GunClassification { Carbine_Rifle, Sniper_Rifle, Shotgun, Energy, Melee, Explosive, Pistol}

    [Header("Defines")]
    public string Name;
    public string Nickname;
    public string Description;
    public RawImage Gunimage;
    public RawImage ScopeImage;
    public GunClassification Class;
    public ProjectileType BulletType;
    public GameObject Bullet;
    [Header("Data")]
    public float Damage;
    public float Firerate;
    public float HitForce;
    public float Range;
    [Header("Ammunition")]
    [Space(10)]
    public int Ammo;
    public int MaxAmmo;
    public int AmmoPerUse;
    public float ReloadSpeed;
    public int ClipsPerUse;
    public int Clips;
    public int MaxClips;
    [Header("Stats")]
    [Space(10)]
    public GunType FiringType;
    public float ShotLength;
    public float Recoil;
    public float ShotRadius;
    [Header("Scope")]
    [Space(10)]
    public bool HasScope;
    public bool CanZoom;
    public float FOV;
}

