﻿using System.Collections;
using UnityEngine;
using UnityEditor;
using UnityEngine.SocialPlatforms;
using Cinemachine.Utility;

public class Gun : MonoBehaviour
{
    [Header("Defines")]
    [Space(10)]
    public Transform PlayerPos;
    public Transform CameraPos;
    public Transform DebugSquare;
    public Transform GunHolder;
    public Transform GunScopedHolder;
    public Transform MuzzlePoint;
    public GameObject MuzzleFlash;
    public ParticleSystem HitFX;
    public GameObject ScopeUI;
    public BoxCollider PlayerMainHitbox;
    public SphereCollider PlayerLegsHitbox;
    public BoxCollider ShotCone;
    public LineRenderer laserLine;
    public TMPro.TMP_Text AmmoCount;
    public TMPro.TMP_Text WeaponStatus;
    public TMPro.TMP_Text ClipCount;
    public UnityEngine.UI.Image Crosshair;
    [Header("Data")]
    [Space(10)]
    public PlayerController PlayerDataIO;
    public GunScriptableObject GunData;
    [Header("Debug")]
    [Space(10)]
    public int ShotCount = 0;
    public Color DebugSquareFireColor;
    public Color DebugSquareBaseColor;
    private Target hitTargetObject;

    [Header("Shooting")]
    [Space(10)]
    public float nextFire;
    public Vector3 Offset;
    public Camera ScopeCam;
    public GameObject ScopeImage;
    public bool DescribeCartridgeLoading = false;
    public bool EnemyDamageTaken = false;
    public Transform CamToUse;

    [Header("Colors")]
    [Space(10)]
    public Color Danger = new Color(255, 0, 0);
    public Color Unknown = new Color(0, 0, 255);
    public Color Good = new Color(0, 255, 0);
    // Update is called once per frame
    void SetCurser()
    {
        if(GunData.Class == GunScriptableObject.GunClassification.Carbine_Rifle || GunData.Class == GunScriptableObject.GunClassification.Pistol)
        {
            Crosshair.sprite = PlayerDataIO.UIsystem.Crosshairs[0];
        }
        else if(GunData.Class == GunScriptableObject.GunClassification.Energy)
        {
            Crosshair.sprite = PlayerDataIO.UIsystem.Crosshairs[1];
        }
        else if (GunData.Class == GunScriptableObject.GunClassification.Melee)
        {
            Crosshair.sprite = PlayerDataIO.UIsystem.Crosshairs[2];
        }
        else if (GunData.Class == GunScriptableObject.GunClassification.Shotgun || GunData.Class == GunScriptableObject.GunClassification.Explosive)
        {
            Crosshair.sprite = PlayerDataIO.UIsystem.Crosshairs[3];
        }
        else if (GunData.Class == GunScriptableObject.GunClassification.Sniper_Rifle)
        {
            Crosshair.sprite = PlayerDataIO.UIsystem.Crosshairs[4];
        }
        else
        {
            Crosshair.sprite = PlayerDataIO.UIsystem.Crosshairs[5];
        }
    }
    void Update()
    {
        SetCurser();
        // Set Debug cube color to cyan (DEBUG)
        string AmmoDiplay = GunData.Ammo.ToString("0");
        string MAxAmmoDiplay = GunData.MaxAmmo.ToString("0");
        ClipCount.text = GunData.Clips.ToString("0");
        if (GunData.Clips > GunData.MaxClips)
        { GunData.Clips = GunData.MaxClips; }
        if (GunData.Clips <= 0)
        { ClipCount.color = Danger; GunData.Clips = 0; }
        else
        { ClipCount.color = Good; }
        if(PlayerDataIO.MainInput.Reload)
        { Reload(GunData.ReloadSpeed); }
        AmmoCount.text = AmmoDiplay + "/" + MAxAmmoDiplay;
        laserLine.SetPosition(0, MuzzlePoint.position);
        if (!PlayerDataIO.MainInput.Shoot_FullAuto || PlayerDataIO.MainInput.Shoot_SemiAuto)
        {
            DebugSquare.gameObject.GetComponent<MeshRenderer>().material.color = DebugSquareBaseColor;
        }
        if (PlayerDataIO.MainInput.Reload)
        {
            Debug.Log("Reload Started");
            WeaponStatus.text = "Reloading";
            WeaponStatus.color = Danger;
            Reload(GunData.ReloadSpeed);
        }
        if (GunData.Ammo <= 0 || GunData.Ammo <= GunData.AmmoPerUse)
        {
            Debug.Log("Reload Started");
            WeaponStatus.text = "Reloading";
            WeaponStatus.color = Danger;
            StartCoroutine(Reload(GunData.ReloadSpeed));
        }
        if (PlayerDataIO.MainInput.Aim && GunData.CanZoom)
        {
            if (GunData.Class != GunScriptableObject.GunClassification.Sniper_Rifle)
            {
                Crosshair.enabled = false;
            }
            CamToUse = ScopeCam.gameObject.transform;
            transform.position = GunScopedHolder.position;
            CameraPos.gameObject.GetComponent<Camera>().enabled = false;
            ScopeCam.enabled = true;
            PlayerDataIO.sensitivity = PlayerDataIO.ScopedSens;
            ScopeCam.fieldOfView = GunData.FOV;
            if (GunData.HasScope)
            {
                ScopeUI.SetActive(true);
            }
        }
        if (!PlayerDataIO.MainInput.Aim && GunData.CanZoom)
        {
            if (GunData.Class != GunScriptableObject.GunClassification.Sniper_Rifle)
            {
                Crosshair.enabled = true;
            }
            CamToUse = CameraPos;
            transform.position = GunHolder.position;
            CameraPos.gameObject.GetComponent<Camera>().enabled = true;
            PlayerDataIO.sensitivity = PlayerDataIO.BaseSens;
            ScopeCam.enabled = false;
            if (GunData.HasScope)
            {
                ScopeUI.SetActive(false);
            }

        }
        // Send out created raycast from player camera
        if (PlayerDataIO.MainInput.Shoot_FullAuto && GunData.FiringType == GunScriptableObject.GunType.FullAuto && Time.time > nextFire && GunData.Ammo > 0 && GunData.BulletType == GunScriptableObject.ProjectileType.RayCast)
        {
            // Cue Line Renderer
            StartCoroutine(ShotEffect(GunData.ShotLength));
            // Play muzzle flash
            MuzzleFlash.GetComponent<ParticleSystem>().Play(true);
            nextFire = Time.time + GunData.Firerate;
            Hit();
        }
        if (Time.time > nextFire)
        {
            WeaponStatus.text = "Loaded";
            WeaponStatus.color = Good;
        }
        else if (DescribeCartridgeLoading)
        {
            WeaponStatus.text = "Loading new Cartidge";
            WeaponStatus.color = Danger;
        }
        if (PlayerDataIO.MainInput.Shoot_SemiAuto && GunData.FiringType == GunScriptableObject.GunType.SemiAuto && Time.time > nextFire && GunData.Ammo > 0 && GunData.BulletType == GunScriptableObject.ProjectileType.RayCast)
        {
            // Cue Line Renderer
            StartCoroutine(ShotEffect(GunData.ShotLength));
            // Play muzzle flash
            MuzzleFlash.GetComponent<ParticleSystem>().Play(true);
            nextFire = Time.time + GunData.Firerate;
            Hit();
        }
        if (PlayerDataIO.MainInput.Shoot_SemiAuto && GunData.FiringType == GunScriptableObject.GunType.SemiAuto && Time.time > nextFire && GunData.Ammo > 0 && GunData.BulletType == GunScriptableObject.ProjectileType.SelfProppeledBullet)
        {
            // Cue Line Renderer
            StartCoroutine(ShotEffect(GunData.ShotLength));
            // Play muzzle flash
            MuzzleFlash.GetComponent<ParticleSystem>().Play(true);
            nextFire = Time.time + GunData.Firerate;
            HitRockets();
        }
        if (PlayerDataIO.MainInput.Shoot_SemiAuto && GunData.FiringType == GunScriptableObject.GunType.SemiAuto && Time.time > nextFire && GunData.Ammo > 0 && GunData.BulletType == GunScriptableObject.ProjectileType.Conecast)
        {
            // Cue Line Renderer
            StartCoroutine(ShotEffect(GunData.ShotLength));
            // Play muzzle flash
            MuzzleFlash.GetComponent<ParticleSystem>().Play(true);
            nextFire = Time.time + GunData.Firerate;
            Debug.Log("Firing");
            HitConeCast();
        }
    }
    private void Start()
    {
        GunData.Clips = GunData.MaxClips;
    }
    void HitRockets()
    {
        GunData.Ammo -= GunData.AmmoPerUse;
        if (GunData.Ammo >= 0)
        {
            GameObject newRocket = Instantiate(GunData.Bullet, MuzzlePoint.position, Quaternion.identity);
        }
        else { Reload(GunData.ReloadSpeed); }
    }
    void HitConeCast()
    {
        RaycastHit hit;
        Physics.Raycast(PlayerDataIO.MainCam.position, PlayerDataIO.MainCam.rotation.eulerAngles, out hit, GunData.Range);
        ShotCone.gameObject.transform.position = Vector3.Lerp(PlayerDataIO.MainCam.position, hit.point, 0.5f);
        ShotCone.enabled = true;
    }
    void Hit()
    {
        // Create Raycast Object (No data yet)
        RaycastHit hit;

        // Use ammo
        GunData.Ammo -= GunData.AmmoPerUse;

        // Draw a debug ray only visible in scene view
        Debug.DrawRay(CamToUse.transform.position, CamToUse.transform.forward * GunData.Range, Good, 1f);
        // Did the raycast hit something?
        if (Physics.Raycast(CamToUse.position, CamToUse.forward, out hit, GunData.Range))
        {
            if (HitFX != null)
            {
                Instantiate(HitFX.gameObject, hit.point, PlayerDataIO.CamController.CamMain.gameObject.transform.rotation);
            }
            if(hit.collider.gameObject.GetComponent<Target>() != null)
            { hitTargetObject = (hit.collider.gameObject.GetComponent<Target>()}

            // Print hit object name
            Debug.Log(hit.collider.gameObject.name);

            // Sets Debug cube to be the directly at the point of impact (DEBUG)
            DebugSquare.position = hit.point;

            // Increase the shot counter
            ShotCount++;

            // Set Debug cube matirial color to red
            DebugSquare.gameObject.GetComponent<MeshRenderer>().material.color = DebugSquareFireColor;

            laserLine.SetPosition(1, hit.point);
            //does the target object have the Rigidbody on it? if so, add force to the facing away from the player
            if (hit.collider.gameObject.GetComponent<Rigidbody>() != null)
            {
                hit.collider.gameObject.GetComponent<Rigidbody>().AddForce(CameraPos.forward * GunData.HitForce);
            }

            // Does the object have a target script on it and is it destroyable
            if (hitTargetObject != null && hitTargetObject.Destructable == true)
            {
                // Set the guns laserline second position to the point of hit
                laserLine.SetPosition(1, hit.point);

                // Is the defense less than or equal to 0, if so then assume defense as 1
                if (hitTargetObject.Defence <= 0)
                { hitTargetObject.Health -= GunData.Damage / 1; }
                // Subract the guns Damage value divided by the Enemies defense value from the targets health
                else
                { hitTargetObject.Health -= GunData.Damage / hitTargetObject.Defence; }

                // Does the enemy have an AI behavior on it?, if so run the Alert code at cs line 48, then check if you shot the trigger range for the enemy
                if (hit.collider.gameObject.GetComponent<EnemyAI>() != null)
                {
                    hit.collider.gameObject.GetComponent<EnemyAI>().AlertEnemy(PlayerPos.gameObject);
                    if (hit.collider.gameObject.GetComponent<EnemyAI>().TriggerRange != null)
                    {
                        if (hit.collider == hit.collider.gameObject.GetComponent<EnemyAI>().TriggerRange)
                        {
                            // Place Recoil onto the gun, then leave loop
                            Vector3 NewNewRotation = new Vector3(-GunData.Recoil, 0, 0);
                            NewNewRotation += PlayerDataIO.PlayerSoul.rotation.eulerAngles;
                            PlayerDataIO.PlayerSoul.eulerAngles = Vector3.Slerp(PlayerDataIO.PlayerSoul.transform.rotation.eulerAngles, NewNewRotation, 1);
                            laserLine.SetPosition(1, CamToUse.forward);
                            return;
                        }
                    }
                }
                // Place Recoil onto the gun, then leave loop
                Vector3 FullAutoNewRotation = new Vector3(-GunData.Recoil, 0, 0);
                FullAutoNewRotation += PlayerDataIO.PlayerSoul.rotation.eulerAngles;
                PlayerDataIO.PlayerSoul.eulerAngles = Vector3.Slerp(PlayerDataIO.PlayerSoul.transform.rotation.eulerAngles, FullAutoNewRotation, 1);
                return;
            }

            // Place Recoil onto the gun
            Vector3 NewRotation = new Vector3(-GunData.Recoil, 0, 0);
            NewRotation += PlayerDataIO.PlayerSoul.rotation.eulerAngles;
            PlayerDataIO.PlayerSoul.eulerAngles = Vector3.Slerp(PlayerDataIO.PlayerSoul.transform.rotation.eulerAngles, NewRotation, 1);
        }
        else
        {
            Vector3 NewRotation = new Vector3(-GunData.Recoil, 0, 0);
            NewRotation += PlayerDataIO.PlayerSoul.rotation.eulerAngles;
            PlayerDataIO.PlayerSoul.eulerAngles = Vector3.Slerp(PlayerDataIO.PlayerSoul.transform.rotation.eulerAngles, NewRotation, 1);
        }

    }

    // Called when ammo is 0, takes a float of reloadTime that should be set in the GunData
    public IEnumerator Reload(float ReloadTime)
    {
        // does the gun have clips to spare?
        if(GunData.Clips > 0)
        {
            // Reset the current clip
            GunData.Ammo = GunData.MaxAmmo;
            WeaponStatus.text = "Loaded";
            WeaponStatus.color = Good;
            GunData.Clips -= GunData.ClipsPerUse;
            Debug.Log("Reloaded full");
            yield return new WaitForSeconds(ReloadTime);
        }
    }
    public IEnumerator ShotEffect(float ShotLength)
    {
        laserLine.enabled = true;

        yield return ShotLength;

        // Deactivate our line renderer after waiting
        laserLine.enabled = false;
    }
}