﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
    public int Coins;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.CompareTag("Player"))
        {
            collision.collider.gameObject.GetComponent<PlayerController>().Cash += Coins;
            Destroy(this.gameObject);
        }
    }
}
