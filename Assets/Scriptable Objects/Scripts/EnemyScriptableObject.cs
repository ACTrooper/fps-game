﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;


[CreateAssetMenu(fileName = "Enemy Data", menuName = "Custom Objects/Enemy Data", order = 2)]
public class EnemyScriptableObject : ScriptableObject
{
    public enum EnemyAttacks { None, Melee, Hitscan, Projectile}
    public enum DeathType { None, DeathParticle, Ragdoll}
    [Header("Defines")]
    public string Name;
    public string Nickname;
    public string Description;
    public GameObject Coin;

    [Header("Data")]
    public DeathType DeathSequence;
    public float Damage;
    public float Health;
    public float HitForce;
    public float ShootingRange;
    public float Range;
    public bool Looks;
    public bool Moves;
    public bool WandersWhenNoPlayer;
    public bool DropsCoins;
    public int minCoins;
    public int maxCoins;
    [Header("Speeds")]
    [Space(10)]
    public float MovementSpeed;
    public float WanderingSpeed;
    [Header("Attacks")]
    public EnemyAttacks EnemyAttackType;

}

