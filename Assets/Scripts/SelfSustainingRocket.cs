﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelfSustainingRocket : MonoBehaviour
{
    public enum ExplosionType { OnImpact, AfterTime}
    public GameObject Player;
    public GameObject DeathParticle;
    public ExplosionType BombType;
    public GunScriptableObject GunData;
    public float ExplosionTime;
    public float Speed;
    public float Damage;
    public bool Homing;
    public bool Explode;
    public Vector3 Offset;
    public float ExplosionRadius;
    public SphereCollider ExplosionRange;
    public Rigidbody rb;
    public int maxColliders;
    public Collider[] numcolliders;
    // Start is called before the first frame update
    private void Awake()
    {

    }
    public void Start()
    {
        Player = GameObject.FindGameObjectWithTag("MainCamera");
        Damage = GunData.Damage;
        ExplosionRange.radius = ExplosionRadius;
        this.gameObject.transform.forward = Player.transform.forward;
        rb.velocity = Vector3.forward * Speed;
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (BombType == ExplosionType.OnImpact)
        {
            if (GetComponent<Collider>().CompareTag("PlayerGun"))
            { return; }
            Debug.Log("Exploded" + gameObject.name);
            Explode = true;
        }
    }
    // Update is called once per frame
    public void Update()
    {
        if(BombType == ExplosionType.AfterTime)
        { StartCoroutine(WaitForExplosion()); }
        if(!Explode)
        { return; }
        //hitColliders = new Collider[maxColliders];
        Debug.Log("Overlap Sphere created");
        numcolliders = Physics.OverlapSphere(gameObject.transform.position, ExplosionRadius);
        for(int i = 0; i > numcolliders.Length; i++)
        {
            
            if (numcolliders[i].gameObject.GetComponent<Target>() != null)
            { this.numcolliders[i].gameObject.GetComponent<Target>().Health -= GunData.Damage; }
            if (this.numcolliders[i].gameObject.GetComponent<Rigidbody>() != null)
            { this.numcolliders[i].gameObject.GetComponent<Rigidbody>().AddExplosionForce(GunData.HitForce, this.gameObject.transform.position, ExplosionRadius); }
            Debug.Log(" GameObject Scanned = " + this.numcolliders[i].gameObject.name);
        }
        Instantiate(DeathParticle, this.gameObject.transform);
        Destroy(gameObject);
    }
    public IEnumerator WaitForExplosion()
    {
        yield return new WaitForSecondsRealtime(ExplosionTime);
        Explode = true;
    }
}
