using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int PastLocRoll;
    public GameObject[] Monsters;
    public int PastMonsterRoll;
    public List<GameObject> SpawningLocations;
    public Transform MonsterParentObject;
    public bool CanSpawn;
    public float Spawnrate;

    void Start()
    {
        SpawnMonster();
    }
    void Update()
    {
        SpawningLocations.RemoveAll(x => x == null);
        if (CanSpawn)
        { SpawnMonster(); }
    }
    public void SpawnMonster()
    {
        int PlaceChance = Random.Range(0, SpawningLocations.Count);
        PastLocRoll = PlaceChance;
        if(SpawningLocations[PlaceChance] == null)
        {  return; }
        int EnemyChance = Random.Range(0, Monsters.Length);
        PastMonsterRoll = EnemyChance;
        GameObject monster = Instantiate(Monsters[EnemyChance], SpawningLocations[PlaceChance].transform.position, Quaternion.identity, MonsterParentObject);
        monster.GetComponent<EnemyAI>().PlayerTextRot = GameObject.Find("Main Camera");
        StartCoroutine(Wait());
    }
    IEnumerator Wait()
    {
        CanSpawn = false;
        yield return new WaitForSecondsRealtime(Spawnrate);
        CanSpawn = true;
    }
}
