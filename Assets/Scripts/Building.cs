﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.PlayerLoop;

public class Building : MonoBehaviour
{
    public BuildingInfo info;
    public GameObject Player;
    public enum GenerationType { Money, None}
    public GenerationType Gentype;
    public int ExtraStorageAmount;
    public int MoneyPerSecond;
    public float Attackrate;
    public int level;
    public bool TurnUsed;

    // Update is called once per frame
    private void Start()
    {
        Updatetooltip();
        Player = GameObject.Find("Player");
    }
    public void Updatetooltip()
    {
        if (this.gameObject.GetComponent<TooltipTriggerObject>() != null)
        {
            this.gameObject.GetComponent<TooltipTriggerObject>().Header = info.Name + " level " + level.ToString() + "/" + info.maxlevel;
            if (level >= info.maxlevel)
            { this.gameObject.GetComponent<TooltipTriggerObject>().Description = info.Description + " Currently this building is at its max level"; }
            else if (level < info.maxlevel)
            { this.gameObject.GetComponent<TooltipTriggerObject>().Description = info.Description +  " Next building level upgrade costs $" + info.UpgradeCosts[level-1];}
            TooltipSystem.Show(this.GetComponent<TooltipTriggerObject>().Header, this.GetComponent<TooltipTriggerObject>().Description);

        }
    }
    void FixedUpdate()
    {
        if(info.GetBuildingType == BuildingInfo.BuildingType.Generation)
        {
            if(TurnUsed == false)
            {
                TurnUsed = true;
                StartCoroutine(Generate());
            }

        }
        if (info.GetBuildingType == BuildingInfo.BuildingType.Attack)
        {
            if (TurnUsed == false)
            {
                TurnUsed = true;
                StartCoroutine(Attack());
            }

        }

    }
    public IEnumerator Attack()
    {

        yield return new WaitForSecondsRealtime(Attackrate);
    }
    public IEnumerator Generate()
    {

        Debug.Log("Started Generation");
        if (Gentype == GenerationType.Money)
        {
            Debug.Log("Waiting...");
            if (Player.GetComponent<PlayerController>() != null)
            {
                Debug.Log("Player Controller is not empty, adding");
                Player.GetComponent<PlayerController>().Cash += MoneyPerSecond * level;
                Debug.Log("Leaving");
                yield return new WaitForSecondsRealtime(2);
                TurnUsed = false;
            }
        }
    }
}
