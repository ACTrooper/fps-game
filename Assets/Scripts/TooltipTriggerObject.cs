﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TooltipTriggerObject : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string Header;
    public string Description;
    public void OnPointerEnter(PointerEventData eventData)
    {
        TooltipSystem.Show(Description, Header);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        TooltipSystem.Hide();
    }
    public void OnMouseEnter()
    {
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            if (this.gameObject.CompareTag("Powerups"))
            {
                Header = this.gameObject.GetComponent<Powerups>().TypeOfPowerup.ToString() + " - " + this.gameObject.GetComponent<Powerups>().SizeOfPowerup.ToString();
            }
            TooltipSystem.Show(Description, Header);
        }
    }
    public void OnMouseExit()
    {
        TooltipSystem.Hide();
    }



}
