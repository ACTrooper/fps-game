﻿using UnityEngine;
using System.Collections;


[CreateAssetMenu(fileName = "Building Data", menuName = "Custom Objects/Building", order = 3)]
public class BuildingInfo : ScriptableObject
{
    public string Name;
    public string Description;
    public int maxlevel;
    public int BuildingCost;
    public int[] UpgradeCosts;
    public enum BuildingType { Generation, Bank, Attack}
    public BuildingType GetBuildingType;
}
