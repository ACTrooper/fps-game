﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class BuildingPlayerAbility : MonoBehaviour
{
    public PlayerController player;
    public GameObject CurrentlySelectedBuilding;
    private bool AlreadyPlaced;
    public GameObject BuildingStorage;
    public int NumberOfBanks;
    public int NumberOfFarms;
    public void Start()
    {
        UpdateValues();    
    }
    public void Update()
    {
        
    }
    public void UpdateValues()
    {
        foreach (Transform child in BuildingStorage.transform)
        {
            if (child.GetComponent<Building>().info.GetBuildingType == BuildingInfo.BuildingType.Bank)
            {
                NumberOfBanks++;
            }
        }
        player.MaxCash = 200 + (NumberOfBanks * 200);
    }
    public void PlaceBuildings()
    {
        if (Input.GetButton("Fire1") && AlreadyPlaced == false)
        {
            AlreadyPlaced = true;
            Ray ray = player.MainCam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
            RaycastHit hit = new RaycastHit();
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.CompareTag("Buidling"))
                    {
                        if (hit.collider.gameObject.GetComponent<Building>().level >= hit.collider.gameObject.GetComponent<Building>().info.maxlevel)
                        {
                            hit.collider.gameObject.GetComponent<Building>().level = hit.collider.gameObject.GetComponent<Building>().info.maxlevel;
                            return;
                        }
                        else if (player.Cash >= hit.collider.gameObject.GetComponent<Building>().info.UpgradeCosts[hit.collider.gameObject.GetComponent<Building>().level-1])
                        {
                            player.Cash -= hit.collider.gameObject.GetComponent<Building>().info.UpgradeCosts[hit.collider.gameObject.GetComponent<Building>().level-1];
                            hit.collider.gameObject.GetComponent<Building>().level++;
                            hit.collider.gameObject.GetComponent<Building>().Updatetooltip();
                            player.UIsystem.Tooltip.GetComponent<Tooltip>().SetText(hit.collider.gameObject.GetComponent<Building>().info.Description, 
                            hit.collider.gameObject.GetComponent<Building>().info.Name + 
                            " level " + 
                            hit.collider.gameObject.GetComponent<Building>().level + 
                            "/" + 
                            hit.collider.gameObject.GetComponent<Building>().info.maxlevel);
                        }
                        return;

                    }
                    if (CurrentlySelectedBuilding.GetComponent<Building>().info.BuildingCost > player.Cash || player.Cash <= 0)
                    {
                        return;
                    }
                    else
                    {
                        player.Cash -= CurrentlySelectedBuilding.GetComponent<Building>().info.BuildingCost;
                        Instantiate(CurrentlySelectedBuilding, hit.point, CurrentlySelectedBuilding.transform.rotation, BuildingStorage.transform);
                        Debug.Log(hit.collider.gameObject.name);
                        UpdateValues();
                    }
                }
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            AlreadyPlaced = false;
        }

    }
    public void SwitchBuildings(GameObject Selectthis)
    {
        CurrentlySelectedBuilding = Selectthis;
    }
}
