﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float Health;
    public float Defence;
    public GameObject DeathEffect;
    public EnemyAI AI;
    public bool Destructable;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.GetComponent<EnemyAI>() != null)
        {
            AI = gameObject.GetComponent<EnemyAI>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Health <= 0)
        {
            if(AI != null)
            {
                AI.Drops();
            }
            if (DeathEffect == null)
            {
                Destroy(this.gameObject, 0);
            }
            else
            {
                GameObject DeathEffectInstantiated = (GameObject)GameObject.Instantiate(DeathEffect, this.transform.position, Quaternion.identity);
                //Instantiate<GameObject>(DeathEffect);
                Destroy(this.gameObject, 0);
            }
        }
    }
}
