﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    [Header("Defines")]
    [Space(10)]
    public Transform Head;
    public Transform Enemy;
    public GameObject Player;
    public GameObject PlayerTextRot;
    public Rigidbody EnemyRigidbody;
    public SphereCollider TriggerRange;
    public GameObject HealthText;
    public GameObject StatusText;
    public SphereCollider HeadCollider;
    public GameObject EnemyWeapon;

    [Header("Data")]
    [Space(10)]
    public EnemyScriptableObject EnemyData;
    public Vector3 HeadOffset;
    public Vector3 TextOffset;
    public Vector3 WeaponOffset;
    private bool Looks = true;
    private bool Moves = true;
    public bool Walking;
    public string[] StatusExpressions;
    public Color[] StatusColors;
    public float Distance;
    public int Chance;
    // Use this for initialization
    void Start()
    {
        if(PlayerTextRot == null)
        {
            PlayerTextRot = GameObject.Find("Main Camera");
        }
        Looks = EnemyData.Looks;
        Moves = EnemyData.Moves;
        if (TriggerRange != null)
        {
            TriggerRange.radius = EnemyData.Range;
        }
    }

    // Update is called once per frame
    public void AlertEnemy(GameObject realPlayer)
    {
        if (StatusText != null)
        {
            StatusText.GetComponent<TMPro.TMP_Text>().text = StatusExpressions[1];
            StatusText.GetComponent<TMPro.TMP_Text>().color = StatusColors[1];
        }
        Player = realPlayer;
    }
    public void Drops()
    {
        if (EnemyData.DropsCoins)
        {
            Chance = Random.Range(EnemyData.minCoins, EnemyData.maxCoins + 1);
            for(int i = 0; i <= Chance; i++)
            {
                Instantiate(EnemyData.Coin, this.gameObject.transform.position, this.gameObject.transform.rotation);
                Debug.Log("Summoned Coin");
            }
        }
    }
    void FixedUpdate()
    {
        Distance = Vector3.Distance(PlayerTextRot.transform.position, this.gameObject.transform.position);
        while(Walking)
        {
            Enemy.transform.Translate(Vector3.forward * EnemyData.MovementSpeed);
        }
        if (EnemyWeapon != null && Player != null)
        {
            EnemyWeapon.transform.LookAt(Player.transform.position);
        }
        HealthText.transform.rotation = PlayerTextRot.transform.rotation;
        if (StatusText != null)
        {
            StatusText.transform.rotation = PlayerTextRot.transform.rotation;
        }
        HealthText.GetComponent<TMPro.TMP_Text>().text = Mathf.Round(this.GetComponent<Target>().Health).ToString();
        EnemyData.Health = this.gameObject.GetComponent<Target>().Health;
        if (Player == null && StatusText != null)
        {
            StatusText.GetComponent<TMPro.TMP_Text>().text = StatusExpressions[0];
            StatusText.GetComponent<TMPro.TMP_Text>().color = StatusColors[0];
        }
        if (Player != null)
        {
            if (Looks)
            {
                Head.transform.rotation = Player.transform.rotation;
                if (EnemyWeapon != null)
                {
                    EnemyWeapon.transform.rotation = Head.rotation;
                }
            }
            if (EnemyData.EnemyAttackType == EnemyScriptableObject.EnemyAttacks.Hitscan || EnemyData.EnemyAttackType == EnemyScriptableObject.EnemyAttacks.Projectile)
            {
                if(Distance < EnemyData.ShootingRange)
                { return; }
                Shoot();
            }
            if (Moves && Player.GetComponent<PlayerController>().Health != 0)
            {
                Enemy.LookAt(new Vector3(Player.transform.position.x, transform.position.y, Player.transform.position.z));
                Enemy.transform.Translate(Vector3.forward * EnemyData.MovementSpeed);
            }
        }
        if(EnemyData.WandersWhenNoPlayer == true)
        {
            int chance = Random.Range(0, 5);
            StartCoroutine(StartWalkForward(chance));
        }
    }
    IEnumerator StartWalkForward(int time)
    {
        Walking = true;
        yield return new WaitForSecondsRealtime(time);
        Walking = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (TriggerRange != null)
        {
            if (other.CompareTag("Player"))
            {
                if (StatusText != null)
                {
                    StatusText.GetComponent<TMPro.TMP_Text>().text = StatusExpressions[1];
                    StatusText.GetComponent<TMPro.TMP_Text>().color = StatusColors[1];
                }
                Player = other.gameObject;
            }
        }
    }
    public void Shoot()
    {
        Debug.Log("Shoot");
        RaycastHit hit;
        //if(Physics.Raycast(Head.transform, EnemyData.Range, out hit))
        //Debug.DrawRay
    }
    private void OnTriggerExit(Collider other)
    {
        if (TriggerRange != null)
        {
            if (other.CompareTag("Player"))
            {
                if (StatusText != null)
                {
                    StatusText.GetComponent<TMPro.TMP_Text>().text = StatusExpressions[0];
                    StatusText.GetComponent<TMPro.TMP_Text>().color = StatusColors[0];
                }
                Player = null;
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if(collision.collider.CompareTag("Player") && !collision.collider.gameObject.GetComponent<PlayerController>().Invincable && EnemyData.EnemyAttackType == EnemyScriptableObject.EnemyAttacks.Melee)
        {
            PlayerController PlayerStats = collision.gameObject.GetComponent<PlayerController>();
            PlayerStats.Health -= EnemyData.Damage;
            PlayerStats.StartCoroutine(PlayerStats.TakeDamage());
            collision.collider.GetComponent<Rigidbody>().AddForce(this.transform.forward * EnemyData.HitForce);
        }
    }
}
