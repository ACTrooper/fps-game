﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TitlescreenManager : MonoBehaviour
{
    public static TitlescreenManager Instance;
    [Header("UI Elements")]
    [Space(10)]
    public Toggle LoadingScreenToggle;
    public Slider SFXSlider;
    public Slider VolumeSlider;
    public Slider SensSlider;
    public Slider ProgressBar;
    public GameObject LoadingScreen;
    public TMPro.TMP_Text ProgressValue;
    public Toggle Fullscreen;
    public TMPro.TMP_InputField Username;
    public TMPro.TMP_Dropdown ResDropdown;
    public TMPro.TMP_Dropdown Quality;
    public TMPro.TMP_Text SensValue;
    public TMPro.TMP_Text VolumeValue;
    public TMPro.TMP_Text NameErrorMessages;
    public GameObject titlescreen;
    public GameObject optionsmenu;
    public GameObject levelselect;
    public Transform PlayButton;
    [Header("Data")]
    [Space(10)]
    public bool UseLoadingScreen;
    public AudioMixer MainAudio;
    public AudioMixer SoundFX;
    public GlobalSettings Data;
    public static int LongestAcceptibleNameLength = 12;
    public Vector3 Offset;
    public Color ErrorMessageColor;
    Resolution[] resolutions;
    public bool IslevelSelectActive;

    [Header("Lean Tween Locations")]
    [Space(10)]
    public Vector3 OriginalOptionsScale = new Vector3(0.9f, 0.9f, 1f);
    public Vector3 OriginalTitlescreenScale = new Vector3(0.7f, 0.7f, 1);
    public Vector3 OriginalLevelSelectScale;

    string Error001 = "Name cannot be more than ";
    string Error002 = "Name cannot be that less than " + LongestAcceptibleNameLength + " characters long.";
    string Sucess001 = "Name saved sucessfully";


    void Awake()
    {
        if (Instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            //Instance = this;
        }
        else if (Instance != this)
        {
            //Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        Debug.Log("Application Loaded");
        if (VolumeSlider != null)
        { VolumeSlider.value = PlayerPrefs.GetFloat("Volume"); }
        if (SensSlider != null)
        { SensSlider.value = PlayerPrefs.GetFloat("sensitivity"); }
        if (Fullscreen != null)
        { Screen.fullScreen = Data.fullscreen; }
        if (Username != null)
        { Username.text = PlayerPrefs.GetString("Username"); }
        if (ResDropdown != null)
        {
            resolutions = Screen.resolutions;
            ResDropdown.ClearOptions();
            List<string> options = new List<string>();
            int currentResolutionIndex = 0;
            for (int i = 0; i < resolutions.Length; i++)
            {
                string option = resolutions[i].width + " x " + resolutions[i].height;
                options.Add(option);
                if (resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
                {
                    currentResolutionIndex = i;
                }
            }
            ResDropdown.AddOptions(options);
            ResDropdown.value = currentResolutionIndex;
            ResDropdown.RefreshShownValue();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(LoadingScreenToggle != null)
        {
            UseLoadingScreen = LoadingScreenToggle.isOn;
        }
        if (ProgressBar != null)
        {
            if(ProgressBar.value <= 0)
            {
                ProgressBar.fillRect.gameObject.SetActive(false);
            }
            else
            {
                ProgressBar.fillRect.gameObject.SetActive(true);
            }
        }
        QualitySettings.SetQualityLevel(Quality.value);
        MainAudio.SetFloat("Volume", VolumeSlider.value);
        SensValue.text = SensSlider.value.ToString("0.0");
        VolumeValue.text = VolumeSlider.value.ToString("0.0");
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            SaveSettings();
        }
    }
    public void ResetSettings()
    {
        PlayerPrefs.DeleteKey("Volume");
        PlayerPrefs.DeleteKey("sensitivity");
        if (VolumeSlider != null)
        { VolumeSlider.value = PlayerPrefs.GetFloat("Volume"); }
        if (SensSlider != null)
        { SensSlider.value = PlayerPrefs.GetFloat("sensitivity"); }
        Fullscreen.isOn = false;
        Username.text = null;
        Data.ClearAllSettings();
    }
    public void StartGame()
    {
        if (IslevelSelectActive == true)
        {
            Debug.Log("Loading Level Select");
            LeanTween.scale(levelselect, OriginalLevelSelectScale, 0.5f);
            IslevelSelectActive = false;
        }
        else
        {
            Debug.Log("Closing Level Select");
            LeanTween.scale(levelselect, Vector3.zero, 0.5f);
            IslevelSelectActive = true;
        }
    }
    public void MultiPlayerStart()
    {
        Debug.Log("Come back when this is completed :)");
    }
    public void Options()
    {
        Debug.Log("Entering Options");
        if (IslevelSelectActive == false)
        {
            Debug.Log("Closing Level Select");
            LeanTween.scale(levelselect, Vector3.zero, 0.5f);
            IslevelSelectActive = true;
        }
        LeanTween.scale(titlescreen, Vector3.zero, 0.5f).setOnComplete(LoadOptionsMenu);
    }
    void LoadOptionsMenu()
    {
        LeanTween.scale(optionsmenu, OriginalOptionsScale, 0.5f);
    }
    public void LeaveOptions()
    {
        Debug.Log("Leaving Options");
        LeanTween.scale(optionsmenu, Vector3.zero, 0.5f).setOnComplete(LoadTitleButtons);
    }
    void LoadTitleButtons()
    {
        LeanTween.scale(titlescreen, OriginalTitlescreenScale, 0.5f);
    }
    public void Quit()
    {
        Debug.Log("Quiting Game");
        Application.Quit();
    }
    public void SaveSettings()
    {
        if (VolumeSlider != null)
        { PlayerPrefs.SetFloat("Volume", VolumeSlider.value); }
        if (SensSlider != null)
        { PlayerPrefs.SetFloat("sensitivity", SensSlider.value); }
        if (Fullscreen != null)
        {
            Data.fullscreen = Fullscreen.isOn;
            Screen.fullScreen = Data.fullscreen;
            GlobalSettings.SetBool("Fullscreen", Fullscreen.isOn);
        }
        if (Username != null)
        {
            if (Username.text.Length <= LongestAcceptibleNameLength)
            {
                if (Username.text.Length != 0)
                {
                    PlayerPrefs.SetString("Username", Username.text);
                    NameErrorMessages.text = Sucess001;
                }
                else
                {
                    NameErrorMessages.text = Error002;
                }
            }
            else
            {
                NameErrorMessages.text = Error001 + LongestAcceptibleNameLength + " characters long";
            }
        }
    }
    public void LoadSceneFromButton(string ScenetobeLoaded)
    {
        if(UseLoadingScreen)
        {
            LoadingScreen.SetActive(true);
            StartCoroutine(LoadNewScene(ScenetobeLoaded));
        }
        else
        {
            SceneManager.LoadScene(ScenetobeLoaded);
        }
    }
    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    public IEnumerator LoadNewScene(string scene)
    {
        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        Debug.Log("Creating Async Operation");
        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        async.allowSceneActivation = false;
        Debug.Log("Finished Creating Async Operation");
        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (async.progress >= 0.9)
        {
            Debug.Log("Loading Not Completed");
            ProgressValue.text = "Loading... Progress is " + async.progress * 100;
            ProgressBar.value = async.progress * 100;
            yield return null;
        }
        ProgressValue.text = "Loading Done!";
        async.allowSceneActivation = true;
    }
}