﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUISystem : MonoBehaviour
{
    PlayerController player;

    [Header("UI Elements")]
    [Space(10)]
    public TMP_Text GameObjectivesRemaining;
    public Image GameObjectiveTypeImage;
    public GameObject PauseMenu;
    public GameObject INTMapUI;
    public Slider HealthSlider;
    public Slider AmmoSlider;
    public Slider EnergySlider;
    public Slider CashSlider;
    public Slider SensSlider;
    public TMP_Text CurseredObjectName;
    public TMP_Text CurseredObjectDesc;
    public TMP_Text ScrollSensValue;
    public TMP_Text HealthText;
    public TMP_Text SpeedText;
    public TMP_Text EnergyText;
    public TMP_Text CashText;
    public Image Changecam;
    public Sprite[] Crosshairs;
    public GameObject Tooltip;
    public bool CanLeave;
    public void Start()
    {
        player = gameObject.GetComponent<PlayerController>();

    }
    public void UpdateUI()
    {
        if(Input.GetButtonUp("Cancel"))
        { CanLeave = true; }
        if (player.MainInput.Leave && CanLeave == true)
        { EscapeMenu(); CanLeave = false; }
        if (!player.MainInput.MapMenuIsOpen)
        { Changecam.color = new Color(150, 0, 0); }
        else
        { Changecam.color = new Color(0, 150, 0); }
        if (HealthSlider.value == 0)
        { HealthSlider.fillRect.gameObject.SetActive(false); }
        else { HealthSlider.fillRect.gameObject.SetActive(true); }
        if (AmmoSlider.value == 0)
        { AmmoSlider.fillRect.gameObject.SetActive(false); }
        else { AmmoSlider.fillRect.gameObject.SetActive(true); }
        if (EnergySlider.value == 0)
        { EnergySlider.fillRect.gameObject.SetActive(false); }
        else { EnergySlider.fillRect.gameObject.SetActive(true); }
        EnergySlider.value = player.Health;
        player.ScrollSens = SensSlider.value;
        float DisplaySens;
        DisplaySens = Mathf.Round(player.ScrollSens * 10) / 10;
        ScrollSensValue.text = DisplaySens.ToString() + "x";
        CashText.text = player.Cash.ToString("0") + "/" + player.MaxCash.ToString("0");
        CashSlider.maxValue = player.MaxCash;
        CashSlider.value = player.Cash;
        SpeedText.text = player.Speed.ToString("0");
        HealthText.text = player.Health.ToString("00") + "/" + player.MaxHealth.ToString("0");
        HealthSlider.maxValue = player.MaxHealth;
        HealthSlider.value = player.Health;
        AmmoSlider.value = player.weapons[Mathf.RoundToInt(player.currentWeapon)].GetComponent<Gun>().GunData.Ammo;
        AmmoSlider.maxValue = player.weapons[Mathf.RoundToInt(player.currentWeapon)].GetComponent<Gun>().GunData.MaxAmmo;
        EnergyText.text = player.Energy.ToString("0") + "/" + player.MaxEnergy.ToString("0");
        EnergySlider.value = player.Energy;
        EnergySlider.maxValue = player.MaxEnergy;
        if (player.MainInput.MapMenuIsOpen)
        { INTMapUI.SetActive(true); }
        else
        { INTMapUI.SetActive(false); }
    }
    public void EscapeMenu()
    {
        player.MainInput.ResetInput(PlayerInput.Resettype.No_Camera);
        LeanTween.scale(PauseMenu, new Vector3(1, 1, 1), 1);
    }
    public void LeaveEscapeMenu()
    {

        player.MainInput.ResetInput(PlayerInput.Resettype.No_Camera);
        player.MainInput.CheckForInput = true;
        LeanTween.scale(PauseMenu,Vector3.zero, 1);
    }
}
