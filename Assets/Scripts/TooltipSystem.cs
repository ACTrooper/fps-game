﻿using UnityEngine;
using System.Collections;

public class TooltipSystem : MonoBehaviour
{
    private static TooltipSystem current;
    public Tooltip tooltip;
    // Use this for initialization
    void Start()
    {
        Hide();
    }
    public void Awake()
    {
        current = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public static void Show(string content, string header)
    {
            current.tooltip.SetText(content, header);
            current.tooltip.gameObject.SetActive(true);
    }
    public static void Hide()
    {
        current.tooltip.gameObject.SetActive(false);
    }
}
